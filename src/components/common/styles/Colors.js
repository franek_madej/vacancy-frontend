import { opacify } from "polished";

export const TwitterBlue = "#55ACEE";
export const FacebookBlue = "#3B5998";
export const SuccessGreen = "#76A81F";

export const WarningRed = "rgb(226, 104, 60)";
export const WarningRedWithOpacity = opacify(-0.25, "rgb(226, 104, 60)");

export const TextBlack = "#000";
export const TextWhite = "#fff";
export const PlaceholderWhite = "rgba(0, 0, 0, 0.75)";
export const InputOrange = "#F9AB00";
export const InputBackground = "rgba(217, 217, 217, 0.3)";

export const ActiveNavigationLink = "#88BF29";
export const HoveredNavigationLink = "rgba(136, 191, 41, 0.5)";
